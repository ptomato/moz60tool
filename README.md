🛠 mozjs-deprecation-tools 🛠
=============================

This is a set of tools for checking your JS source code when you upgrade
to the next extended-support release (ESR) of SpiderMonkey, to see if
you are using any Mozilla-specific extensions that will be removed.

- Upgrading from ESR 52 to ESR 60? Use `moz60tool`.
- Upgrading from ESR 60 to ESR 68? Use `moz68tool`.

To run it (replace `moz60tool` with whichever one you are using):
```
./moz60tool myfile.js
```

There are no command line arguments and it only processes one file.
However, if you want to batch-process files you can use `find`:
```
find /path/to/mycode -name '*.js' -exec ./moz60tool {} \;
```

The tool is only intended to be used once, at the time when you migrate
your codebase.
It's not meant for use in CI pipelines.

Oh no, my distro has already upgraded to GJS with the new SpiderMonkey
----------------------------------------------------------------------
As a workaround, it _should_ work to run the tool with the JS
development shell binary which is probably shipped with the devel
package from the previous SpiderMonkey ESR.
In the case of `moz60tool`, that would be the `js52` binary shipped with
whatever your distro's equivalent of `mozjs52-devel` is.
```
js52 ./moz60tool myfile.js
```

Maintenance Status
------------------
No maintenance intended, but if you submit a merge request I will look
at it.
