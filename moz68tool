#!/usr/bin/env gjs

var filename;
var script;
var numErrors = 0;

// cf. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Deprecated_String_generics
const deprecated_String_generics = new Set([
    'charAt',
    'charCodeAt',
    'concat',
    'contains',
    'endsWith',
    'includes',
    'indexOf',
    'lastIndexOf',
    'localeCompare',
    'match',
    'normalize',
    'replace',
    'search',
    'slice',
    'split',
    'startsWith',
    'substr',
    'substring',
    'toLocaleLowerCase',
    'toLocaleUpperCase',
    'toLowerCase',
    'toUpperCase',
    'trim',
    'trimLeft',
    'trimRight'
]);

if (typeof imports !== 'undefined') {
    window.System = imports.system;

    const {Gio} = imports.gi;
    const ByteArray = imports.byteArray;
    filename = ARGV[0];
    const file = Gio.File.new_for_commandline_arg(filename);
    [, script] = file.load_contents(null);
    script = ByteArray.toString(script, 'UTF-8');
} else {
    // Running under SpiderMonkey devel shell
    /* global os, quit, scriptArgs, System: true */
    filename = scriptArgs[0];
    script = os.file.readFile(filename);
    System = {
        exit(code) {
            quit(code);
        }
    };
}

function walk(node, func) {
    func(node);
    nodesToWalk(node).forEach(n => walk(n, func));
}

function nodesToWalk(node) {
    switch(node.type) {
    case 'ArrayPattern':
    case 'BreakStatement':
    case 'CallSiteObject':  // i.e. strings passed to template
    case 'ContinueStatement':
    case 'DebuggerStatement':
    case 'EmptyStatement':
    case 'Identifier':
    case 'Literal':
    case 'MetaProperty':  // i.e. new.target
    case 'Super':
    case 'ThisExpression':
        return [];
    case 'ArrowFunctionExpression':
    case 'FunctionDeclaration':
    case 'FunctionExpression':
        return [...node.defaults, node.body].filter(n => !!n);
    case 'AssignmentExpression':
    case 'BinaryExpression':
    case 'ComprehensionBlock':
    case 'LogicalExpression':
        return [node.left, node.right];
    case 'ArrayExpression':
    case 'TemplateLiteral':
        return node.elements.filter(n => !!n);
    case 'BlockStatement':
    case 'Program':
        return node.body;
    case 'CallExpression':
    case 'NewExpression':
    case 'TaggedTemplate':
        return [node.callee, ...node.arguments];
    case 'CatchClause':
        return [node.body, node.guard].filter(n => !!n);
    case 'ClassExpression':
    case 'ClassStatement':
        return [...node.body, node.superClass].filter(n => !!n);
    case 'ClassMethod':
        return [node.name, node.body];
    case 'ComprehensionExpression':
    case 'GeneratorExpression':
        return [node.body, ...node.blocks, node.filter].filter(n => !!n);
    case 'ComprehensionIf':
        return [node.test];
    case 'ComputedName':
        return [node.name];
    case 'ConditionalExpression':
    case 'IfStatement':
        return [node.test, node.consequent, node.alternate].filter(n => !!n);
    case 'DoWhileStatement':
    case 'WhileStatement':
        return [node.body, node.test];
    case 'ExportDeclaration':
        return [node.declaration, node.source].filter(n => !!n);
    case 'ImportDeclaration':
        return [...node.specifiers, node.source];
    case 'LetStatement':
        return [...node.head, node.body];
    case 'ExpressionStatement':
        return [node.expression];
    case 'ForInStatement':
    case 'ForOfStatement':
        return [node.body, node.left, node.right];
    case 'ForStatement':
        return [node.init, node.test, node.update, node.body].filter(n => !!n);
    case 'LabeledStatement':
        return [node.body];
    case 'MemberExpression':
        return [node.object, node.property];
    case 'ObjectExpression':
    case 'ObjectPattern':
        return node.properties;
    case 'Property':
    case 'PrototypeMutation':
        return [node.value];
    case 'ReturnStatement':
    case 'ThrowStatement':
    case 'UnaryExpression':
    case 'UpdateExpression':
    case 'YieldExpression':
        return node.argument ? [node.argument] : [];
    case 'SequenceExpression':
        return node.expressions;
    case 'SpreadExpression':
        return [node.expression];
    case 'SwitchCase':
        return [node.test, ...node.consequent].filter(n => !!n);
    case 'SwitchStatement':
        return [node.discriminant, ...node.cases];
    case 'TryStatement':
        return [node.block, node.handler, node.finalizer].filter(n => !!n);
    case 'VariableDeclaration':
        return node.declarations;
    case 'VariableDeclarator':
        return node.init ? [node.init] : [];
    case 'WithStatement':
        return [node.object, node.body];
    default:
        print(`Ignoring ${node.type}, you should probably fix this in the script`);
        return [];
    }
}

function warn(loc, why, bad, good) {
    print(`${loc.source}:${loc.start.line}:${loc.start.column}: ${why}`);
    print(`  WRONG:   ${bad}`);
    print(`  CORRECT: ${good}`);
    numErrors++;
}

function findDeprecated(node) {
    switch(node.type) {
    case 'CallExpression':
        if (node.callee.type === 'MemberExpression' &&
            node.callee.object.type === 'Identifier' &&
            node.callee.object.name === 'String' &&
            node.callee.property.type === 'Identifier' &&
            deprecated_String_generics.has(node.callee.property.name)) {
            if (node.arguments.length == 1)
                warn(node.loc, 'String generics used.',
                    `String.${node.callee.property.name}(s)`,
                    `s.${node.callee.property.name}()`);
            else
                warn(node.loc, 'String generics used.',
                    `String.${node.callee.property.name}(s, ...)`,
                    `s.${node.callee.property.name}(...)`);
        }
        break;
    }
}

print('Scanning', filename);

if (script.startsWith('#!'))
    script = script.replace(/^#!.*$/m, '');

var ast = Reflect.parse(script, {source: filename});
walk(ast, findDeprecated);
print(`${numErrors} error${numErrors === 1 ? '' : 's'} found.`);
System.exit(numErrors > 0);
